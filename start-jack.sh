#!/bin/bash
pd -stderr -nogui \
-path /usr/lib/pd/extra/lua -lib lua \
-path /usr/lib/pd/extra/Gem -lib Gem \
-open VISUALS.pd &
pd -rt -jack -inchannels 0 -outchannels 2 -audiobuf 100 -stderr \
-path /usr/lib/pd/extra/lua -lib lua \
-path /usr/lib/pd/extra/Gem -lib Gem \
-path /usr/lib/pd/extra/gridflow -lib gridflow \
-open LOADER.pd \
>> "/home/claude/tmp/lac-$(date --iso=s).log" 2>&1
