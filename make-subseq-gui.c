#include <stdio.h>

int main(void) {
  const char *labels[24] = {
    "kick-1",
    "kick-2",
    "snare-1",
    "snare-2",
    "clap",
    "hihat",
    "unused",
    "unused",
    "unused",
    "unused",
    "sync-1",
    "unused",
    "flute-1",
    "flute-2",
    "hyper-1",
    "hyper-2",
    "unused",
    "bass-2",
    "organ-1",
    "organ-2",
    "organ-3",
    "organ-4",
    "sparkle-1",
    "sparkle-2"
  };
  printf("#N canvas 0 0 450 300 10;\n");
  printf("#X obj 106 9 receive \\$1;\n");
  printf("#X obj 106 29 subseq-gui-guts \\$0;\n");
  printf("#X obj 106 50 send \\$2;\n");
  for (int i = 0; i < 24; ++i) {
    int y = 28 * i + 11;
    printf("#X obj 18 %d cnv 5 16 14 empty \\$0-%d-0-c empty 20 12 0 14 -195568 -66577 0;\n", y, i);
    printf("#X obj 34 %d cnv 5 16 14 empty \\$0-%d-1-c empty 20 12 0 14 -195568 -66577 0;\n", y, i);
    printf("#X obj 50 %d cnv 5 16 14 empty \\$0-%d-2-c empty 20 12 0 14 -195568 -66577 0;\n", y, i);
    printf("#X obj 66 %d cnv 5 16 14 empty \\$0-%d-3-c empty 20 12 0 14 -195568 -66577 0;\n", y, i);
    printf("#X obj 19 %d cnv 5 63 13 empty \\$0-%d-c %s 2 6 1 8 -195568 -33289 0;\n", y + 15, i, labels[i]);
    printf("#X obj 4 %d vradio 14 1 0 2 \\$0-%d-seq-s \\$0-%d-seq-r empty 0 -8 0 10 -195568 -1 -1 0;\n", y, i, i);
    printf("#X floatatom 18 %d 2 1 6 0 - #0-%d-div-r #0-%d-div-s;\n", y, i, i);
    printf("#X floatatom 34 %d 2 1 64 0 - #0-%d-mod1-r #0-%d-mod1-s;\n", y, i, i);
    printf("#X floatatom 50 %d 2 1 64 0 - #0-%d-mod2-r #0-%d-mod2-s;\n", y, i, i);
    printf("#X floatatom 66 %d 2 0 63 0 - #0-%d-start-r #0-%d-start-s;\n", y, i, i);
  }
  printf("#X connect 0 0 1 0;\n");
  printf("#X connect 1 0 2 0;\n");
  printf("#X coords 0 -1 1 1 80 674 2 3 10;\n");
  return 0;
}
