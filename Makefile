all: numarray.pd subseq-gui.pd

numarray.pd: make-numarray
	./make-numarray > numarray.pd

make-numarray: make-numarray.c
	gcc -std=c99 -Wall -pedantic -o make-numarray make-numarray.c

subseq-gui.pd: make-subseq-gui
	./make-subseq-gui > subseq-gui.pd

make-subseq-gui: make-subseq-gui.c
	gcc -std=c99 -Wall -pedantic -o make-subseq-gui make-subseq-gui.c
