local Guts = pd.Class:new():register("numarray-guts")

function Guts:initialize(name, atoms)
  if nil == atoms[1] then
    return false
  end
  self.dollarzero = atoms[1]
  self.cursor = 0;
  self.receives = { }
  self.inlets = 1
  self.outlets = 1
  return true
end

function Guts:in_1_list(atoms)
  pd.send(self.dollarzero .. "-" .. atoms[1] .. "-r", "float", { atoms[2] })
end

function Guts:in_1_set(atoms)
  pd.send(self.dollarzero .. "-" .. atoms[1] .. "-r", "set", { atoms[2] })
end

function Guts:colour(i)
  local h = 6 * i / 3072
  local s
  local v
  if math.mod(           i,        4) == 0 then s = 1 else s = 0.5 end
  if math.mod(math.floor(i / 128), 2) == 0 then v = 1 else v = 0.7 end
  local r
  local g
  local b
  local k = math.floor(h)
  local f = h - k
  local p = v * (1 - s)
  local q = v * (1 - s * f)
  local t = v * (1 - s * (1 - f))
  if k == 0 then r = v; g = t; b = p; else
  if k == 1 then r = q; g = v; b = p; else
  if k == 2 then r = p; g = v; b = t; else
  if k == 3 then r = p; g = q; b = v; else
  if k == 4 then r = t; g = p; b = v; else
  if k == 5 then r = v; g = p; b = q; end; end; end; end; end; end
  r = math.floor(255*r)
  g = math.floor(255*g)
  b = math.floor(255*b)
  return (-1 - 65536 * r - 256 * g - b)
end

function Guts:in_1_cursor(atoms)
  if 0 <= atoms[1] and atoms[1] < 3072 then
    local c
    c = self:colour(self.cursor)
    pd.send(self.dollarzero .. "-" .. self.cursor .. "-c", "color", { c, c })
    pd.send(self.dollarzero .. "-" .. atoms[1]    .. "-c", "color", { 0, 0 })
    self.cursor = atoms[1]
  end
end

function Guts:postinitialize()
  local i = 0
  while (i < 3072) do
    local n = i
    self.receives[i+1] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-s", "receive_" .. n
    )
    self["receive_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, atoms[1] })
    end
    i = i + 1
  end
end

function Guts:finalize()
  for _,r in ipairs(self.receives) do r:destruct() end
end
