#!/bin/bash
mkfifo stream.yuv
mplayer -vo yuv4mpeg -ao null out.ogv &
cat stream.yuv |
y4mscaler -O preset=dvd_wide |
mpeg2enc -f 8 -a 3 -b 8500 -o video.m2v
mplayer -vc null -vo null -ao pcm:fast out.ogv
twolame -b 224 audiodump.wav audio.m2a
mplex -f 8 -V -o final.mpeg audio.m2a video.m2v
