#!/bin/bash
pd -stderr -nogui -lib lua:Gem -open VISUALS.pd &
pd -rt -alsa -audiodev 6 -r 48000 -inchannels 0 -outchannels 2 -audiobuf 100 \
-stderr -path . -lib lua:Gem:gridflow -open LOADER.pd \
>> "/home/claude/tmp/lac-$(date --iso=s).log" 2>&1
