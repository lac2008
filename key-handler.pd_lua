------------------------------------------------------------------------------
-- Helper functions
------------------------------------------------------------------------------
local wrap = function (x, m)
  if x >= m then return x - m else
  if x <  0 then return x + m else
    return x
  end end
end
------------------------------------------------------------------------------
-- Keyboard Input Handler
------------------------------------------------------------------------------
local Keys = pd.Class:new():register("key-handler")
function Keys:initialize(name, atoms)
  ----------------------------------------------------------------------------
  -- modifier keys
  ----------------------------------------------------------------------------
  self.modifiers = {
    shift  = false, shiftl = false, shiftr = false,
    alt    = false, altl   = false, altr   = false,
    ctrl   = false, ctrll  = false, ctrlr  = false,
    win    = false, winl   = false, winr   = false,
    caps   = false
  }
  self.modifierKeys  = {
    Shift_L          = function(ctx, state) ctx:doModifier(state, "shift", "l") end,
    Shift_R          = function(ctx, state) ctx:doModifier(state, "shift", "r") end,
    Control_L        = function(ctx, state) ctx:doModifier(state, "ctrl",  "l") end,
    Control_R        = function(ctx, state) ctx:doModifier(state, "ctrl",  "r") end,
    Alt_L            = function(ctx, state) ctx:doModifier(state, "alt",   "l") end,
    ISO_Level3_Shift = function(ctx, state) ctx:doModifier(state, "alt",   "r") end,
    Super_L          = function(ctx, state) ctx:doModifier(state, "win",   "l") end,
    Menu             = function(ctx, state) ctx:doModifier(state, "win",   "r") end,
    Caps_Lock        = function(ctx, state) ctx.modifiers.caps = state          end
  }
  self.doModifier    = function(ctx, state, name, which)
    ctx.modifiers[name .. which] = state
    ctx.modifiers[name] = ctx.modifiers[name .. "l"] or ctx.modifiers[name .. "r"]
  end
  ----------------------------------------------------------------------------
  -- global keys
  ----------------------------------------------------------------------------
  self.globalKeys    = {
    Escape           = function(ctx)
      if ctx.modifiers.winl then ctx:outlet(1, "go", { 1 }) else
      if ctx.modifiers.winr then ctx:outlet(1, "go", { 0 }) end end
    end,
    Pause            = function(ctx) ctx:doMode("sound") end,
    Print            = function(ctx) ctx:doMode("mixer") end,
    Insert           = function(ctx) ctx:doMode("sbseq") end,
    Delete           = function(ctx) ctx:doMode("notes") end
  }
  self.mode          = "notes"
  self.modeKeys      = { }
  self.doMode        = function(ctx, name)
    ctx.modeKeys = ctx[name .. "Keys"]
    self:outlet(1, "keyboardMode", { name })
  end
  ----------------------------------------------------------------------------
  -- sound keys
  ----------------------------------------------------------------------------
  self.soundKeys     = {
  }
  ----------------------------------------------------------------------------
  -- mixer keys
  ----------------------------------------------------------------------------
  self.mixerKeys     = {
  }
  ----------------------------------------------------------------------------
  -- sbseq keys
  ----------------------------------------------------------------------------
  self.sbseqKeys     = {
    Left             = function(ctx) ctx:doMoveSeqCursor("X", -1) end,
    Right            = function(ctx) ctx:doMoveSeqCursor("X",  1) end,
    Up               = function(ctx) ctx:doMoveSeqCursor("Y", -1) end,
    Down             = function(ctx) ctx:doMoveSeqCursor("Y",  1) end,
    F1               = function(ctx) ctx:doSetSeqCursor( 0) end,
    F2               = function(ctx) ctx:doSetSeqCursor( 1) end,
    F3               = function(ctx) ctx:doSetSeqCursor( 2) end,
    F4               = function(ctx) ctx:doSetSeqCursor( 3) end,
    F5               = function(ctx) ctx:doSetSeqCursor( 4) end,
    F6               = function(ctx) ctx:doSetSeqCursor( 5) end,
    F7               = function(ctx) ctx:doSetSeqCursor( 6) end,
    F8               = function(ctx) ctx:doSetSeqCursor( 7) end,
    F9               = function(ctx) ctx:doSetSeqCursor( 8) end,
    F10              = function(ctx) ctx:doSetSeqCursor( 9) end,
    F11              = function(ctx) ctx:doSetSeqCursor(10) end,
    F12              = function(ctx) ctx:doSetSeqCursor(11) end,
    D0               = function(ctx) ctx:doSeqDigit(0) end,
    D1               = function(ctx) ctx:doSeqDigit(1) end,
    D2               = function(ctx) ctx:doSeqDigit(2) end,
    D3               = function(ctx) ctx:doSeqDigit(3) end,
    D4               = function(ctx) ctx:doSeqDigit(4) end,
    D5               = function(ctx) ctx:doSeqDigit(5) end,
    D6               = function(ctx) ctx:doSeqDigit(6) end,
    D7               = function(ctx) ctx:doSeqDigit(7) end,
    D8               = function(ctx) ctx:doSeqDigit(8) end,
    D9               = function(ctx) ctx:doSeqDigit(9) end,
    Return           = function(ctx) ctx:doSeqDigitClear() end,
    Tab              = function(ctx) ctx:doToggleSeq() end
  }
  self.seqHaveDigit = false
  self.seqDigit = 0
  self.seqCursorX = 0 ; self.seqCursorXwrap = 4
  self.seqCursorY = 0 ; self.seqCursorYwrap = 24
  self.seqLabel = { "div", "mod1", "mod2", "start" }
  self.doMoveSeqCursor = function(ctx, which, dir)
    ctx["seqCursor" .. which] = wrap(
      ctx["seqCursor" .. which] + dir,
      ctx["seqCursor" .. which .. "wrap"]
    )
    ctx:doOutletSeqCursor()
  end
  self.doSetSeqCursor = function(ctx, value)
    local shift
    if ctx.modifiers.shift then shift = 12 else shift = 0 end
    ctx["seqCursorY"] = value + shift
    ctx:doOutletSeqCursor()
  end
  self.doOutletSeqCursor = function(ctx, foo)
    ctx.noteCursorY = ctx.seqCursorY
    ctx:outlet(1, "seqCursor", { ctx.seqCursorY, ctx.seqCursorX })
    if not foo then ctx:doOutletNoteCursor(ctx, true) end
  end
  self.doSeqDigit = function (ctx, n)
    if ctx.seqHaveDigit then
      ctx.seqHaveDigit = false
      ctx:outlet(1, "seqEntry", {
        ctx.seqCursorY, ctx.seqLabel[ctx.seqCursorX + 1], ctx.seqDigit * 10 + n
      })
    else
      ctx.seqDigit = n
      ctx.seqHaveDigit = true
    end
  end
  self.doSeqDigitClear = function(ctx)
    self.seqHaveDigit = false
    self.seqDigit = 0
  end
  self.doToggleSeq = function(ctx)
    ctx:outlet(1, "seqToggle", { ctx.seqCursorY })
  end
  ----------------------------------------------------------------------------
  -- notes keys
  ----------------------------------------------------------------------------
  self.notesKeys     = {
    -- cursor manipulation
    Left             = function(ctx) ctx:doMoveNoteCursor("X", -1) end,
    Right            = function(ctx) ctx:doMoveNoteCursor("X",  1) end,
    Up               = function(ctx) ctx:doMoveNoteCursor("Y", -1) end,
    Down             = function(ctx) ctx:doMoveNoteCursor("Y",  1) end,
    Tab              = function(ctx) ctx:doMoveNoteCursor("Z",  1) end,
    F1               = function(ctx) ctx:doSetNoteCursor( 0) end,
    F2               = function(ctx) ctx:doSetNoteCursor( 1) end,
    F3               = function(ctx) ctx:doSetNoteCursor( 2) end,
    F4               = function(ctx) ctx:doSetNoteCursor( 3) end,
    F5               = function(ctx) ctx:doSetNoteCursor( 4) end,
    F6               = function(ctx) ctx:doSetNoteCursor( 5) end,
    F7               = function(ctx) ctx:doSetNoteCursor( 6) end,
    F8               = function(ctx) ctx:doSetNoteCursor( 7) end,
    F9               = function(ctx) ctx:doSetNoteCursor( 8) end,
    F10              = function(ctx) ctx:doSetNoteCursor( 9) end,
    F11              = function(ctx) ctx:doSetNoteCursor(10) end,
    F12              = function(ctx) ctx:doSetNoteCursor(11) end,
    -- note entry transpose
    Prior            = function(ctx) ctx:doNoteTranspose( 1) end,
    Next             = function(ctx) ctx:doNoteTranspose(-1) end,
    -- note entry
    space            = function(ctx) ctx:doNote(0) end,
    _fallback        = function(ctx, key)
      if ctx.key2note[key] then ctx:doNote(ctx.key2note[key] + ctx.noteTranspose) end
    end
  }
  self.noteCursorX = 0 ; self.noteCursorXwrap = 64 ; self.noteCursorXstep = 1
  self.noteCursorY = 0 ; self.noteCursorYwrap = 24 ; self.noteCursorYstep = 1
  self.noteCursorZ = 0 ; self.noteCursorZwrap = 2  ; self.noteCursorZstep = 1
  self.noteTranspose = 36 ; self.noteTransposeMin = 12 ; self.noteTransposeMax = 72
  self.key2note = {
    z = 0, s = 1, x = 2, d = 3, c = 4, v = 5, g = 6, b = 7, h = 8, n = 9, j = 10, m = 11,
    comma = 12, l = 13, period = 14, semicolon = 15, slash = 16,
    q = 12, D2 = 13, w = 14, D3 = 15, e = 16, r = 17, D5 = 18, t = 19, D6 = 20, y = 21, D7 = 22, u = 23,
    i = 24, D9 = 25, o = 26, D0 = 27, p = 28, bracketleft = 29, equal = 30, bracketright = 31
  }
  self.doMoveNoteCursor = function(ctx, which, dir, noshift)
    if not noshift and ctx.modifiers.shift and which == "X" then
      ctx.noteCursorXstep = math.min(math.max(ctx.noteCursorXstep + dir, 0), 8)
      ctx:outlet(1, "noteCursorStep", { ctx.noteCursorXstep })
    else
      ctx["noteCursor" .. which] = wrap(
        ctx["noteCursor" .. which] + dir * ctx["noteCursor" .. which .. "step"],
        ctx["noteCursor" .. which .. "wrap"]
      )
      ctx:doOutletNoteCursor()
    end
  end
  self.doSetNoteCursor = function(ctx, value)
    local which
    local shift
    local multiplier
    if ctx.modifiers.ctrl then
      which = "Y"
      if ctx.modifiers.shift then shift = 12 else shift = 0 end
      multiplier = 1
    else
      which = "X"
      if ctx.modifiers.shift then shift =  4 else shift = 0 end
      multiplier = 8
    end
    ctx["noteCursor" .. which] = wrap(
      value * multiplier + shift,
      ctx["noteCursor" .. which .. "wrap"]
    )
    ctx:doOutletNoteCursor()
  end
  self.doOutletNoteCursor = function(ctx, foo)
    ctx.seqCursorY = ctx.noteCursorY
    ctx:outlet(1, "noteCursor", {
     (ctx.noteCursorY * ctx.noteCursorZwrap + ctx.noteCursorZ) * 
     ctx.noteCursorXwrap + ctx.noteCursorX
    })
    if not foo then ctx:doOutletSeqCursor(ctx, true) end
  end
  self.doNoteTranspose = function(ctx, dir)
    local step
    if ctx.modifiers.shift then step = 1 * dir else step = 12 * dir end
    ctx.noteTranspose = math.max(math.min(
      ctx.noteTranspose + step,
      ctx.noteTransposeMax),
      ctx.noteTransposeMin
    )
    ctx:outlet(1, "noteTranspose", { ctx.noteTranspose })
  end
  self.doNote = function(ctx, note)
    ctx:outlet(1, "note", {
      (ctx.noteCursorY * ctx.noteCursorZwrap + ctx.noteCursorZ) *
      ctx.noteCursorXwrap + ctx.noteCursorX,
      note
    })
    ctx:doMoveNoteCursor("X", 1, true)
  end
  ----------------------------------------------------------------------------
  -- handle keyboard input
  ----------------------------------------------------------------------------
  self.in_1_key      = function(ctx, atoms)
    local state = (atoms[1] > 0)
    local key   =  atoms[2]
    if      ctx.modifierKeys[key] then
            ctx.modifierKeys[key](ctx, state)
    else if ctx.globalKeys[key] and state then
            ctx.globalKeys[key](ctx)
    else if ctx.modeKeys[key] and state then
            ctx.modeKeys[key](ctx)
    else if ctx.modeKeys._fallback and state then
            ctx.modeKeys._fallback(ctx, key)
    end end end end
  end
  ----------------------------------------------------------------------------
  -- object properties
  ----------------------------------------------------------------------------
  self.inlets = 1
  self.outlets = 1
  return true
end
------------------------------------------------------------------------------
-- laptop keyboard layout
------------------------------------------------------------------------------
--[[
Escape F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12 Pause Break Insert Delete
grave D1 D2 D3 D4 D5 D6 D7 D8 D9 D0 minus equal BackSpace Home
Tab q w e r t y u i o p bracketleft bracketright Return Prior
Caps_Lock a s d f g h j k l semicolon apostrophe numbersign Next
Shift_L backslash z x c v b n m comma period slash Shift_R Up End
Control_L Super_L Alt_L space ISO_Level3_Shift Menu Control_R Left Down Right
--]]
------------------------------------------------------------------------------
-- end of file
------------------------------------------------------------------------------
