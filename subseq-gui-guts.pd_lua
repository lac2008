local Guts = pd.Class:new():register("subseq-gui-guts")

function Guts:initialize(name, atoms)
  if nil == atoms[1] then
    return false
  end
  self.dollarzero = atoms[1]
  self.cursorX = 0
  self.cursorY = 0
  self.receives = { }
  self.inlets = 1
  self.outlets = 1
  return true
end

function Guts:in_1_cursor(atoms)
  pd.send(self.dollarzero .. "-" .. self.cursorY .. "-c", "color", { 1, 22 })
  pd.send(self.dollarzero .. "-" .. self.cursorY .. "-" .. self.cursorX .. "-c", "color", { 1, 1 })
  self.cursorY = atoms[1]
  self.cursorX = atoms[2]
  pd.send(self.dollarzero .. "-" .. self.cursorY .. "-c", "color", { 0, 22 })
  pd.send(self.dollarzero .. "-" .. self.cursorY .. "-" .. self.cursorX .. "-c", "color", { 0, 0 })
end

-- temporarily redirect the radio listener, then bang it to get the current value
-- then cancel the redirect, and modify the radio value
function Guts:in_1_toggle(atoms)
  local seq = atoms[1]
  local receive_saved = self["receive_seq_" .. seq]
  self["receive_seq_" .. seq] = function (self, sel, atoms)
    self["receive_seq_" .. seq] = receive_saved
    pd.send(self.dollarzero .. "-" .. seq .. "-seq-r", "float", { 1 - atoms[1] })
  end
  pd.send(self.dollarzero .. "-" .. seq .. "-seq-r", "bang", { })
end

function Guts:in_1_list(atoms)
  pd.send(self.dollarzero .. "-" .. atoms[1] .. "-" .. atoms[2] .. "-r", "float", { atoms[3] })
end

function Guts:in_1_set(atoms)
  pd.send(self.dollarzero .. "-" .. atoms[1] .. "-" .. atoms[2] .. "-r", "set", { atoms[3] })
end

function Guts:postinitialize()
  local i = 0
  while (i < (24*5)) do
    local n = math.floor(i / 5)
    self.receives[i+1] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-div-s", "receive_div_" .. n
    )
    self["receive_div_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, "div", atoms[1] })
    end
    self.receives[i+2] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-mod1-s", "receive_mod1_" .. n
    )
    self["receive_mod1_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, "mod1", atoms[1] })
    end
    self.receives[i+3] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-mod2-s", "receive_mod2_" .. n
    )
    self["receive_mod2_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, "mod2", atoms[1] })
    end
    self.receives[i+4] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-start-s", "receive_start_" .. n
    )
    self["receive_start_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, "start", atoms[1] })
    end
    self.receives[i+5] = pd.Receive:new():register(
      self, self.dollarzero .. "-" .. n .. "-seq-s", "receive_seq_" .. n
    )
    self["receive_seq_" .. n] = function (self, sel, atoms)
      self:outlet(1, "list", { n, "seq", atoms[1] })
    end
    i = i + 5
  end
end

function Guts:finalize()
  for _,r in ipairs(self.receives) do r:destruct() end
end
