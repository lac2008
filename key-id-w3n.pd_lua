local w3n_key2id = {
  Escape = 1, F1 = 2, F2 = 3, F3 = 4, F4 = 5, F5 = 6, F6 = 7, F7 = 8, F8 = 9, F9 = 10, F10 = 11, F11 = 12, F12 = 13, Pause = 14, Break = 15, Insert = 16, Delete = 17,
  grave = 18, D1 = 19, D2 = 20, D3 = 21, D4 = 22, D5 = 23, D6 = 24, D7 = 25, D8 = 26, D9 = 27, D0 = 28, minus = 29, equal = 30, BackSpace = 31, Home = 32,
  Tab = 33, q = 34, w = 35, e = 36, r = 37, t = 38, y = 39, u = 40, i = 41, o = 42, p = 43, bracketleft = 44, bracketright = 45, Return = 46, Prior = 47,
  Caps_Lock = 48, a = 49, s = 50, d = 51, f = 52, g = 53, h = 54, j = 55, k = 56, l = 57, semicolon = 58, apostrophe = 59, numbersign = 60, Next = 61,
  Shift_L = 62, backslash = 63, z = 64, x = 65, c = 66, v = 67, b = 68, n = 69, m = 70, comma = 71, period = 72, slash = 73, Shift_R = 74, Up = 75, End = 76,
  function_key = 77, Control_L = 78, Super_L = 79, Alt_L = 80, space = 81, ISO_Level3_Shift = 82, Menu = 83, Control_R = 84, Left = 85, Down = 86, Right = 87
}
local w3n_id2key = { }
local k
local v
for k,v in pairs(w3n_key2id) do
  w3n_id2key[v] = k
end
local W3N = pd.Class:new():register("key-id-w3n")
function W3N:initialize(name, atoms)
  self.inlets = 1
  self.outlets = 1
  return true
end
function W3N:in_1_symbol(s)
  self:outlet(1, "float", { w3n_key2id[s] - 1 })
end
function W3N:in_1_float(f)
  self:outlet(1, "symbol", { w3n_id2key[f + 1] })
end
