#!/bin/bash
pd -stderr -noaudio -nogui -lib lua:Gem -open VISUALS.pd &
pd -rt -oss -r 48000 -inchannels 0 -outchannels 2 -audiobuf 100 \
-stderr -path . -lib lua:Gem:gridflow -open LOADER.pd \
>> "/home/claude/tmp/lac-$(date --iso=s).log" 2>&1
