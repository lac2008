local KAD = pd.Class:new():register("key-array-decay")
function KAD:initialize(name, atoms)
  self.state = { }
  self.value = { }
  self.decay = 0.9
  local count = atoms[1]
  while count > 0 do
    self.state[count] = false
    self.value[count] = 0
    count = count - 1
  end
  self.inlets = 1
  self.outlets = 1
  return true
end
function KAD:in_1_list(atoms)
  local k = atoms[1]
  local v = atoms[2]
  self.state[k] = (v > 0)
  if v > 0 then self.value[k] = 1 end
end
function KAD:in_1_float(f)
  self:outlet(1, "float", { self.value[f] })
end
function KAD:in_1_bang()
  local k
  local v
  for k,v in pairs(self.state) do
    if not v then self.value[k] = self.value[k] * self.decay end
  end
end
