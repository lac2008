/*
#N canvas 0 0 450 300 10;
#X obj 6 9 inlet;
#X obj 6 29 +;
#X obj 6 50 outlet;
#X obj 115 94 cnv 5 16 14 empty \$0-r empty 20 12 0 14 -258699 -258699
0;
#X floatatom 115 94 2 -9 99 0 - #0-r #0-s;
#X obj 131 94 cnv 5 16 14 empty \$0-r empty 20 12 0 14 -258699 -258699
0;
#X floatatom 131 94 2 -9 99 0 - #0-r #0-s;
#X obj 115 80 cnv 5 16 14 empty \$0-r empty 20 12 0 14 -258699 -258699
0;
#X floatatom 115 80 2 -9 99 0 - #0-r #0-s;
#X obj 131 80 cnv 5 16 14 empty \$0-r empty 20 12 0 14 -258699 -258699
0;
#X floatatom 131 80 2 -9 99 0 - #0-r #0-s;
#X connect 0 0 1 0;
#X connect 1 0 2 0;
*/

#include <stdio.h>

int main(void) {
  printf("#N canvas 0 0 450 300 10;\n");
  printf("#X obj 6 9 receive \\$1;\n");
  printf("#X obj 6 29 numarray-guts \\$0;\n");
  printf("#X obj 6 50 send \\$2;\n");
  for (int j = 0; j < 48; ++j) {
    for (int i = 0; i < 64; ++i) {
      int n = 64 * (47 - j) + i;
      int x = 16 *       i  + 100;
      int y = 14 * (47 - j) + 100;
      printf("#X obj %d %d cnv 5 16 14 empty \\$0-%d-c empty 20 12 0 14 0 0 0;\n", x, y, n);
      printf("#X floatatom %d %d 2 -9 99 0 - #0-%d-r #0-%d-s;\n", x, y, n, n);
    }
  }
  printf("#X connect 0 0 1 0;\n");
  printf("#X connect 1 0 2 0;\n");
  printf("#X coords 0 -1 1 1 1026 674 2 99 99;\n");
  return 0;
}
